<?php


function text_startWith( $text, $needle ){
    
    return substr($text, 0, strlen($needle)) == $needle;

}


