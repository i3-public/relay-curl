<?php


function try_content( $url ){
	
	$headers = get_headers($url);
	$code = explode(' ', $headers[0])[1];

	if( in_array($code, [301, 302]) ){
		foreach( $headers as $header ){
			if( text_startWith($header, 'Location: ') ){
				$url = substr($header, 10);
				return try_content($url);
			}
		}
		die;

	} else {
		
		foreach( $headers as $header ){
			if( text_startWith($header, 'Content-Type: ') ){
				$content_type = substr($header, 14);
				break;
			}
		}
		$content_type = strtolower($content_type);

		if( strstr($content_type, 'url') ){

			foreach( $headers as $header )
				if(! in_array( explode(': ', $header)[0] , ['Content-Disposition', 'Content-Length']) )
					header($header);

			m3u8_fix_n_echo($url, $content_type);
		
		} else if( strstr($content_type, 'video') or $content_type='application/dash+xml' ){

			foreach( $headers as $header )
				if(! in_array( explode(': ', $header)[0] , ['Content-Disposition']) )
					header($header);

			get_video_by_url($url, $content_type);

		} else {
			echo "cant find the type of content: {$content_type}";
		}

	}

}



function m3u8_fix_n_echo( $url ){

	$m3u8 = curl($url);

	$m3u8_arr = explode("\n", $m3u8);
	foreach( $m3u8_arr as $i => $line ){
		if( $line = trim($line, "\r\n\t ") ){
			
			# global replace
			$m3u8_arr[$i] = str_replace("http", "http://".$_SERVER['HTTP_HOST']."/http", $line);

			# if at the first of line
			if(! text_startWith($line, '#') ){
				if(! text_startWith($line, 'http') ){
					$m3u8_arr[$i] = "http://".$_SERVER['HTTP_HOST']."/".url_globalpath($url, $line);
				}
			}

			# if extra attr

		}
	}
	
	$m3u8 = implode("\n", $m3u8_arr);
	header('Content-Disposition: attachment; filename="'.get_filename_by_url($url).'"');
	header('Content-Length: '.strlen($m3u8));
	echo $m3u8;	

}



function get_video_by_url( $url ){
	
	header('Content-Disposition: attachment; filename="'.get_filename_by_url($url).'"');

	$fp = fopen($url, 'rb');
	while(! feof($fp) )
		echo stream_get_contents($fp, 1024);
	fclose($fp);

}


function get_filename_by_url( $url ){
	
	$url = explode('?', $url)[0];
	$file = strrchr($url, '/');
	$file = substr($file, 1);

	return $file;
}


function url_globalpath( $url, $line ){

	if( substr($line, 0, 1) == '/' ){
		$url = explode('/', $url);
		$url = $url[0].'//'.$url[2].$line;

	} else {
		$url = explode("?", $url)[0];
		if( substr($url, -1) != '/' ){
			$file = substr(strrchr($url, '/'), 1);
			$url = substr($url, 0, -1*strlen($file));
		}
		$url = $url.$line;
	}

	return $url;
}

