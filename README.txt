
# main port: 1960
# wget -qO- https://gitlab.com/i3-public/relay-curl/-/raw/main/README.txt | bash

wget -O dockerfile https://gitlab.com/i3-public/conf/-/raw/main/nginx-on-docker/conf/dockerfile
sed -i 's,#INJECT,RUN wget -qO- https://gitlab.com/i3-public/relay-curl/-/raw/main/conf/patch.sh | sh,g' dockerfile

docker rm -f relay-curl
docker rmi relay-curl

docker build -t relay-curl -f dockerfile .
docker run -t -d --restart unless-stopped --name relay-curl -p 1922:22 -p 1960:80 relay-curl
