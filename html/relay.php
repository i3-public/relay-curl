<?php

foreach( glob('../inc/*.php') as $file )
	include_once($file);



ini_set('display_errors', 'on');
error_reporting(E_ALL & ~E_NOTICE);
ini_set('memory_limit', -1);



$_GET = [];

if(! $url = substr($_SERVER['REQUEST_URI'], 1) ){
	echo 'no url defined';

} else if( substr($url, 0, 4) != 'http' ){
	echo 'wrong address!';
	
} else {
	try_content($url);
}


