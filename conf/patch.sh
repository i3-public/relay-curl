cd /var
rm -rf www
git clone https://gitlab.com/i3-public/relay-curl.git
mv relay-curl www

wget -O /etc/nginx/sites-available/default https://gitlab.com/i3-public/relay-curl/-/raw/main/conf/default
service nginx reload

wget -qO- https://gitlab.com/i3-public/relay-curl/-/raw/main/conf/cron.txt > /var/spool/cron/crontabs/root

wget https://gitlab.com/i3-public/net-usage/-/raw/master/README.md -O ~/net-usage-installer.sh
bash ~/net-usage-installer.sh 1960 skipserver
